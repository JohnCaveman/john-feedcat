$(document).ready(function() {


  $('.delLink').click(function (event) {

 
    event.preventDefault();

    var answer = confirm("Do you want to proceed deleting this issue?");
    if (answer) {
    	$.ajax({
    		type : "GET",
    		url : 'deleteIssue',
    		data : {
    			issueID : $(this).attr("id")
    		},
    		success : function() {
    		
    			window.location.reload(true);
    			 
    			
    		}
    	});
    }
  });

});