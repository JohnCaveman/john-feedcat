function reloadTrendsChartIn(year) {

	$
			.get(
					"/aggregateStatusTrends/" + year,

					function(seriesData) {

						var seriesDataTemplate = [ {
							name : 'new',
							data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
						}, {
							name : 'open',
							data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
						}, {
							// Not a typo, cuz in DB we got this... need to be
							// fixed
							name : 'inProgess',
							data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
						}, {
							name : 'resolved',
							data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
						} ];

						// Scan seriesData and modify corresponding month count
						seriesData
								.forEach(function(group) {
									seriesDataTemplate
											.forEach(function(series) {
												if (group._id.status == series.name)
													series.data[group._id.month - 1] = group.count;
											});
								});

						// console.log(seriesDataTemplate);

						// Create chart, FIXME: does this new leads to a memory
						// leak in
						// browser? use $().highchart
						new Highcharts.Chart(
								{
									chart : {
										renderTo : 'coloumnChart',
										type : 'column'
									},
									title : {
										text : 'Monthly Issues Trends by status'
									},
									subtitle : {
										text : ''
									},
									xAxis : {
										categories : [ 'Jan', 'Feb', 'Mar',
												'Apr', 'May', 'Jun', 'Jul',
												'Aug', 'Sep', 'Oct', 'Nov',
												'Dec' ]
									},
									yAxis : {
										min : 0,
										title : {
											text : 'Issues'
										}
									},
									tooltip : {
										headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
										pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
												+ '<td style="padding:0"><b>{point.y}</b></td></tr>',
										footerFormat : '</table>',
										shared : true,
										useHTML : true
									},
									plotOptions : {
										column : {
											pointPadding : 0.2,
											borderWidth : 0
										}
									},
									exporting : {
										buttons : {
											contextButton : {
												menuItems : [
														{
															text : 'Export to PNG',
															onclick : function() {
																this
																		.exportChart({
																			filename : 'trend-'
																					+ year
																		});
															}
														},
														{
															text : 'Export to PDF',
															onclick : function() {
																this
																		.exportChart({
																			type : 'application/pdf',
																			filename : 'trend-'
																					+ year
																		});
															}
														} ]
											}
										}
									},

									series : seriesDataTemplate
								});
					});
}

$(document).ready(function() {
	// Register reload callback
	$("#trends-year-input").keypress(function(e) {
		if (e.which == 13) {
			reloadTrendsChartIn($(this).val())
		}
	});
	// First Reload
	reloadTrendsChartIn(2013);
});