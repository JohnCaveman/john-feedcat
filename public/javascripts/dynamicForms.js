$(document).ready(function() {
	// $("#zero").css("display", "visible");
	$("#user").css("display", "none");
	$("#customer").css("display", "none");
	$("#advancedSearchDiv").css("display", "none");


//	$(document).ready(function () {
//	    $('select').selectList();   
//	  });

		     
		

	$(function() {
		$( "#date" ).datepicker({ dateFormat: 'dd/mm/yy' });
	});

	$(".issueType").click(function() { // Radio Button !!
		if ($('input[name=issueType]:checked').val() == "Zero-Result") {
			$("#zero").slideDown("fast"); // Slide Down Effect
		} else {
			$("#zero").slideUp("fast"); // Slide Up Effect
		}

		if ($('input[name=issueType]:checked').val() == "User-Feedback") {
			$("#user").slideDown("fast");
		} else {
			$("#user").slideUp("fast");
		}

		if ($('input[name=issueType]:checked').val() == "Customer-Feedback") {
			$("#customer").slideDown("fast");
		} else {
			$("#customer").slideUp("fast");
		}

	});
	

	


	$(function() {

		var addDiv = $('#addinput');
		var j=0;

		var i = $('#addinput p').size() + 1;

		 
		$('#addNew').live('click', function() { if(i%2!=0){
		$('<p><li class="by-other"><div class="avatar pull-right"></div><div class="chat-content"><div class="chat-meta">Further Communication <span class="pull-right"><a href="#" id="remNew">Remove</a> </span></div>'+
		    '<textarea class="span12" id="customerFeedback.initialCommunication'+"["+(i)+"]"+'" name="customerFeedback.initialCommunication'+"["+(i)+"]"+'" placeholder="Initial Communication" rows="6"  style="resize:none;" ></textarea><div class="clearfix"></div></div></li></p>').appendTo(addDiv);
		} else {
		$('<p><li class="by-me"><div class="avatar pull-right"></div><div class="chat-content"><div class="chat-meta">Further Communication <span class="pull-right"><a href="#" id="remNew">Remove</a> </span></div>'+
		        '<textarea class="span12" id="customerFeedback.initialCommunication'+"["+(i)+"]"+'" name="customerFeedback.initialCommunication'+"["+(i)+"]"+'" placeholder="Initial Communication" rows="6" style="resize:none;" ></textarea><div class="clearfix"></div></div></li></p>').appendTo(addDiv);        
		}

		i++;
		return false;

		});
		
		 
		$('#remNew').live('click', function() {
		if( i > 1 ) {
			
		$(this).parents('p').remove();
		
		i--;
		}
		return false;
		});
		});
	
	
	
	
	


});

$(".classification").change(function() {

	$.ajax({
		type : "GET",
		url : 'updateClassification',
		data : {
			issueID : $(this).attr("data-id"),
			classification : $(this).val()
		},
		success : function(data) {

		}
	});

});



// Visualisation graph ... pie chart !!

$(function() {

	var data = [];
	var series = 4;
	var a = $("#highPriVal").val();
	var b = $("#lowPriVal").val();
	var c = $("#medPriVal").val();
	var d = $("#unPriVal").val();
	var total = parseInt(a) + parseInt(b) + parseInt(c) + parseInt(d);
	
	var pa = Math.round ((parseInt(a) *100 )/ total);
	var pb = Math.round ((parseInt(b) *100 )/ total);
	var pc = Math.round ( ( parseInt(c)*100 )/ total);
	var pd = Math.round ((parseInt(d)*100) / total);

	data[0] = {
		label : "High Priority "+ pa+"%",
		data : parseInt(a)
	}


	data[1] = {
		label : "Low Priority "+ pb+"%",
		data : parseInt(b)
	}
	data[2] = {
		label : "Medium Priority "+ pc+"%",
		data : parseInt(c)
	}
	data[3] = {
		label : "Un Prioritised "+ pd+"%",
		data : parseInt(d)
	}

	$.plot($("#pie-chart2"), data, {
		series : {
			pie : {
				show : true
			}
		},
		grid : {
			hoverable : true
		}
	});

});

