        var pageNum = 1;
/**
* Reset numbering on tab buttons
*/
function reNumberPages() {
    pageNum = 1;
    var tabCount = $('#pageTab > li').length;
    $('#pageTab > li').each(function() {
        var pageId = $(this).children('a').attr('href');
        if (pageId == "#page1") {
            return true;
        }
        pageNum++;
        $(this).children('a').html('Communication ' + pageNum +
            '<button class="close" type="button" ' +
            'title="Remove this page">x</button>');
    });
}
        
        $(document).ready(function() {
                /**
                 * Add a Tab
                 */
                $('#btnAddPage').click(function() {
                pageNum++;
                $('#pageTab').append(
                        $('<li><a href="#page' + pageNum + '">' +
                        'Communication ' + pageNum +
                        '<button class="close" type="button" ' +
                        'title="Remove this page">x</button>' +
                        '</a></li>'));

                $('#pageTabContent').append(
                        $('<div class="tab-pane" id="page' + pageNum +
                        '"><textarea class="span12" id="customerFeedback.initialCommunication[' +(pageNum-1)+']" name="customerFeedback.initialCommunication[' +(pageNum-1)+']" placeholder="Further Communications" rows="10" ></textarea></div>'));

                $('#page' + pageNum).tab('show');
                });

/**
* Remove a Tab
*/
$('#pageTab').on('click', ' li a .close', function() {
var tabId = $(this).parents('li').children('a').attr('href');
$(this).parents('li').remove('li');
$(tabId).remove();
reNumberPages();
$('#pageTab a:first').tab('show');
});

/**
 * Click Tab to show its content 
 */
$("#pageTab").on("click", "a", function(e) {
e.preventDefault();
$(this).tab('show');
});
});
  