/**
 * Created by John Caveman on 15th Oct 2013
 * 
 * Should learn some JQuery to minimize code.
 */
$(document).ready(function() {

	// Bind datepickers, ok button
	$("#reload-classification-btn").click(
			function() {

				renderBarChart($("#classification-start-date").val(), $(
						"#classification-end-date").val());
			});

	$("#classification-start-date").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#classification-end-date").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	

	renderBarChart("2013-01-01", "2014-01-01");
});

function renderBarChart(startDate, endDate) {

	// Clear chart
	d3.select("#real-bar-chart").remove();
	

	d3.json("/aggregateCategory/" + startDate + "/" + endDate, function(data) {
		
		if( !data ) {
			console.log("[Error] Barchart receive empty data from server");
			return;
		}

		var valueLabelWidth = 20; // space reserved for value labels (right)
		var barHeight = 35; // height of one bar
		var barLabelWidth = 300; // space reserved for bar labels
		var barLabelPadding = 20; // padding between bar and bar labels (left)
		var gridLabelHeight = 25; // space reserved for gridline labels
		var gridChartOffset = 10; // space between start of grid and first bar
		var maxBarWidth = 600; // width of the bar with the max value

		// accessor functions
		var barLabel = function(d) {
			return d['_id'];
		};
		var barValue = function(d) {
			return parseFloat(d['total']);
		};

		// scales
		var yScale = d3.scale.ordinal().domain(d3.range(0, data.length))
				.rangeBands([ 0, data.length * barHeight ]);
		var y = function(d, i) {
			return yScale(i);
		};
		var yText = function(d, i) {
			return y(d, i) + yScale.rangeBand() / 2;
		};
		var x = d3.scale.linear().domain([ 0, d3.max(data, barValue) ]).range(
				[ 0, maxBarWidth ]);
		// svg container element
		var chart = d3.select('#john-bar-chart').append("svg").attr('id', 'real-bar-chart').attr('width',
				maxBarWidth + barLabelWidth + valueLabelWidth).attr('height',
				gridLabelHeight + gridChartOffset + data.length * barHeight);
		// grid line labels
		var gridContainer = chart.append('g').attr('transform',
				'translate(' + barLabelWidth + ',' + gridLabelHeight + ')');
		// gridContainer.selectAll("text").data(x.ticks(10)).enter().append("text")
		// .attr("x", x).attr("dy", -3).attr("text-anchor", "middle").text(
		// String);
		// vertical grid lines
		gridContainer.selectAll("line").data(x.ticks(10)).enter()
				.append("line").attr("x1", x).attr("x2", x).attr("y1", 0).attr(
						"y2", yScale.rangeExtent()[1] + gridChartOffset).style(
						"stroke", "#ccc").style("stroke-width", "1");
		// bar labels
		var labelsContainer = chart.append('g').attr(
				'transform',
				'translate(' + (barLabelWidth - barLabelPadding) + ','
						+ (gridLabelHeight + gridChartOffset) + ')');
		labelsContainer.selectAll('text').data(data).enter().append('text')
				.attr('y', yText).attr('stroke', 'none').attr('fill', 'black')
				.attr("dy", ".35em") // vertical-align: middle
				.attr('text-anchor', 'end').text(barLabel);
		// bars
		var barsContainer = chart.append('g').attr(
				'transform',
				'translate(' + barLabelWidth + ','
						+ (gridLabelHeight + gridChartOffset) + ')');
		barsContainer.selectAll("rect").data(data).enter().append("rect").attr(
				'y', y).attr('height', yScale.rangeBand()).attr('width',
				function(d) {
					return x(barValue(d));
				}).attr('stroke', 'white').attr('fill', '#43C6DB');
		// bar value labels
		barsContainer.selectAll("text").data(data).enter().append("text").attr(
				"x", function(d) {
					return x(barValue(d));
				}).attr("y", yText).attr("dx", 10) // padding-left
		.attr("dy", ".35em") // vertical-align: middle
		.attr("text-anchor", "start") // text-align: right
		.attr("fill", "black").attr("stroke", "none").text(function(d) {
			return d3.round(barValue(d), 2);
		});
		// start line
		barsContainer.append("line").attr("y1", -gridChartOffset).attr("y2",
				yScale.rangeExtent()[1] + gridChartOffset).style("stroke",
				"#000");
	});
}

function initGoogleMap() {

	var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng(
			-10, 112), new google.maps.LatLng(-44.5, 155.5));

	var mapOptions = {
		center : new google.maps.LatLng(-24.8, 133.6),
		mapTypeId : google.maps.MapTypeId.ROADMAP,

		streetViewControl : false,
		panControl : true,
		panControlOptions : {
			position : google.maps.ControlPosition.RIGHT_CENTER
		},
		zoomControl : true,
		zoomControlOptions : {
			position : google.maps.ControlPosition.RIGHT_CENTER
		},

		scrollwheel : false
	};

	/** Make this global * */
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

	map.fitBounds(defaultBounds);

	var searchBox = setupSearchBox(map);

	/** Heatmap * */
	var json = d3.json("/getWeightedHeatPoints", function(data) {

		var issueData = new Array();

		// Filter docs which does not have a geo field

		data.forEach(function(wp) {
			if (wp["_id"] != null) {

				issueData.push({
					location : new google.maps.LatLng(wp["_id"][0],
							wp["_id"][1]),
					weight : wp["weiget"]
				});
			}

		});

		/** make this global * */
		heatPointMVCArray = new google.maps.MVCArray(issueData);

		/** Make this global * */
		heatmap = new google.maps.visualization.HeatmapLayer({
			data : heatPointMVCArray,
			radius : 81
		});

		heatmap.setMap(map);
	});

	/** Event listeners * */
	google.maps.event.addListener(map, 'bounds_changed', function() {
		var bounds = map.getBounds();
		searchBox.setBounds(bounds);
	});

	var infoWindow = new google.maps.InfoWindow();

	google.maps.event
			.addListener(
					map,
					'rightclick',
					function(event) {

						var option = {
							content : "<button type='button' class='btn btn-default btn-xs' id='latLng'>"
									+ event.latLng.toString() + "</button>",
							position : event.latLng
						};

						infoWindow.close();
						infoWindow.setOptions(option);
						infoWindow.open(map);

						// TODO: copy into clipboard, maybe later to jump to
						// create issue page
						// JQuery copy plugin required, learn some JQuery
						$("#latLng").click(function() {
							alert($("#latLng").text());
						});
					});

	google.maps.event.addListener(map, 'click', function(evnet) {

		infoWindow.close();
	});
}

function setupSearchBox(map) {

	// Create the search box and link it to the UI element.
	var input = document.getElementById('search-text');
	var searchBox = new google.maps.places.SearchBox(input);
	var markers = [];

	// Listen for the event fired when the user selects an item from the
	// pick list. Retrieve the matching places for that item.
	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();

		for (var i = 0, marker; marker = markers[i]; i++) {
			marker.setMap(null);
		}

		// For each place, get the icon, place name, and location.
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, place; place = places[i]; i++) {
			var image = {
				url : place.icon,
				size : new google.maps.Size(71, 71),
				origin : new google.maps.Point(0, 0),
				anchor : new google.maps.Point(17, 34),
				scaledSize : new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			var marker = new google.maps.Marker({
				map : map,
				icon : image,
				title : place.name,
				position : place.geometry.location
			});

			markers.push(marker);

			bounds.extend(place.geometry.location);
		}

		map.fitBounds(bounds);
		map.setZoom(15);
	});

	return searchBox;
}

function toggleHeatmap() {
	heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeZoomLevel() {
	var curRadius = heatmap.get('radius');
	heatmap.setOptions({
		radius : curRadius === 9 ? 81 : curRadius / 3
	});
}

google.maps.event.addDomListener(window, 'load', initGoogleMap);
