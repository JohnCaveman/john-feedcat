function reloadReporterBtw(startDate, endDate) {

	$
			.get(
					"/aggregateReporterWeekly/" + startDate + "/" + endDate,
					function(seriesData) {
						
						// contains name, series
						var seriesSet = {};
						// read in data, find match in seriesSet, create new entry if cannot found
						// combine year.week to a double value time
						// push [time, count] into seriesSet.found.series
						seriesData.forEach( function(ea) {
							
							var time = ea._id.time;
							
							if( !seriesSet[ea._id.reporter] ) {
								seriesSet[ea._id.reporter] = [[time, ea.count]];
							} else {
								seriesSet[ea._id.reporter].push([time, ea.count]);
							}
						});
												
						var seriesList = [];
						
						for( key in seriesSet ) {
							seriesList.push( { 'name' : key, 'data' : seriesSet[key]} )
						}
						
						console.log(seriesList)
						

						new Highcharts.Chart(
								{
									chart : {
										renderTo : 'reporter-history-chart',
									},
									title : {
										text : 'Reporter History'
									},
									subtitle : {
										text : ''
									},
									xAxis : {
										title : {
											text : 'week of year'
										},
										labels : {
											formatter : function() {
												return this.value;
											}
										}
									},
									yAxis : {
										title : {
											text : 'No. of Issues Reported'
										},
										labels : {
											formatter : function() {
												return this.value;
											}
										}
									},
									tooltip : {
										pointFormat : '{series.name} reported <b>{point.y:,.0f}</b><br/>issues in {point.x}'
									},
									plotOptions : {
										area : {
											marker : {
												enabled : false,
												symbol : 'circle',
												radius : 2,
												states : {
													hover : {
														enabled : true
													}
												}
											}
										}
									},
									exporting : {
										buttons : {
											contextButton : {
												menuItems : [
														{
															text : 'Export to PNG',
															onclick : function() {
																this
																		.exportChart({
																			filename : 'reporters from'
																					+ startDate
																					+ ' to '
																					+ endDate
																		});
															}
														},
														{
															text : 'Export to PDF',
															onclick : function() {
																this
																		.exportChart({
																			type : 'application/pdf',
																			filename : 'reporters from'
																					+ startDate
																					+ ' to '
																					+ endDate
																		});
															}
														} ]
											}
										}
									},

									series : seriesList
								});
					});
}

$(document).ready(
		function() {

			$("#reload-reporter-btn").click(
					function() {

						reloadReporterBtw($("#reporter-start-date").val(), $(
								"#reporter-end-date").val());
					});

			$("#reporter-start-date").datepicker({
				dateFormat : 'yy-mm-dd'
			});
			$("#reporter-end-date").datepicker({
				dateFormat : 'yy-mm-dd'
			});

			reloadReporterBtw( "2013-01-01", "2014-01-01");

		});