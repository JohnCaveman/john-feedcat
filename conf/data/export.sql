CREATE DATABASE sensis;

USE sensis;

CREATE TABLE user (
  User_Name varchar(255) NOT NULL,
  Password varchar(255) NOT NULL,
  User_Type varchar(255) NOT NULL,
  First_Name varchar(255),
  Last_Name varchar(255),
  PRIMARY KEY (User_Name)
)