package models

import java.util.Date

import scala.collection.mutable.ArrayBuffer

import com.mongodb.DBObject
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.MongoConnection
import com.mongodb.casbah.commons.MongoDBObject
import play.api.libs.ws.WS

case class Issue(_id: ObjectId,
  issueID: String,
  issueType: String,
  summary: String,
  status: String,
  classification: List[String],
  date: java.util.Date, // Changed 
  priority: String,
  lastUpdated: java.util.Date,
  crossReferenceID: String,
  attachments: List[String], 
  action: String,
  zeroResults: ZeroResults,
  userFeedback: UserFeedback,
  customerFeedback: CustomerFeedback)

case class ZeroResults(query: Option[String],
  location: Option[String],
  count: Option[Int], 
  percentViews: Option[Double])

case class UserFeedback(query: Option[String],
  location: Option[String],
  count: Option[Int], 
  reporter: Option[String],
  problemSource: Option[String],
  endUserFeedback: Option[String],
  searchEngineerFeedback: Option[String],
  npsScore: Option[Int], //changed
  verbatimFeedback: Option[String],
  feedbackClass: Option[String])

case class CustomerFeedback(query: Option[String],
  location: Option[String],
  reporter: Option[String],
  initialCommunication: Option[List[String]],
  estimatedCost: Option[String],
  listingsEffected: Option[String],
  advertiserID: Option[String],
  liaison: Liaison)

case class Liaison(name: Option[String],
  email: Option[String],
  role: Option[String],
  dept: Option[String])

case class Counter(
  _id: ObjectId,
  seq: Int)

object Issue {

  def getNextSequence(): Option[Int] = {
    val counter = MongoConnection()("sensis")("counter")
    val query = MongoDBObject("_id" -> "issueNo")
    val fields = MongoDBObject()
    val sort = MongoDBObject("seq" -> 1)
    val update = $inc("seq" -> 1)
    //val result = counter.update( query, update , upsert=true)

    counter.findAndModify(query, fields, sort, false, update, true, true).asInstanceOf[Option[BasicDBObject]].get.getAs[Int]("seq")//.asInstanceOf[BasicDBObject].getAs[Int]("seq")
  }

  def printIssue(issue: Issue): Boolean = {
    println("trying to print")
    println(issue)
    true
  }

  def saveCustomerFeedback(issue: Issue, issueCollection: MongoCollection): Boolean = {
    
    val newIssueDocument = MongoDBObject("_id" -> new ObjectId,
      "issueID" -> issue.issueID,
      "issueType" -> issue.issueType,
      "summary" -> issue.summary,
      "status" -> issue.status,
      "classification" -> issue.classification,
      "date" -> issue.date,
      "priority" -> issue.priority,
      "lastUpdated" -> new java.util.Date(),
      "crossReferenceID" -> issue.crossReferenceID,
      "attachments" -> issue.attachments,
      "action" -> issue.action,
      "customerFeedback" -> MongoDBObject("query" -> issue.customerFeedback.query.getOrElse("no_query"),
        "location" -> issue.customerFeedback.location.getOrElse("no_location"),
        "reporter" -> issue.customerFeedback.reporter.getOrElse("no_reporter"),
        "initialCommunication" -> issue.customerFeedback.initialCommunication.getOrElse(List("No Communication Recorded.")),
        "estimatedCost" -> issue.customerFeedback.estimatedCost.getOrElse("no_estimated_cost"),
        "listingsEffected" -> issue.customerFeedback.listingsEffected.getOrElse("no_listings_effected"),
        "advertiserID" -> issue.customerFeedback.advertiserID.getOrElse("no_advertiserID"),
        "liaison" -> MongoDBObject("name" -> issue.customerFeedback.liaison.name,
          "email" -> issue.customerFeedback.liaison.email,
          "role" -> issue.customerFeedback.liaison.role,
          "dept" -> issue.customerFeedback.liaison.dept)))
    issueCollection.save(newIssueDocument) // Have to loop through number of newIssue in CSV file.
  
    true
  }

  def saveZeroFeedback(issue: Issue, issueCollection: MongoCollection): Boolean = {
    val newIssueDocument = MongoDBObject("_id" -> new ObjectId,
      "issueID" -> issue.issueID,

      "issueType" -> issue.issueType,
      "summary" -> issue.summary,
      "status" -> issue.status,
      "classification" -> issue.classification,
      "date" -> issue.date,
      "priority" -> issue.priority,
      "lastUpdated" -> new java.util.Date(),
      "crossReferenceID" -> issue.crossReferenceID,
      "attachments" -> issue.attachments,
      "action" -> issue.action,
      "zeroResult" -> MongoDBObject("query" -> issue.zeroResults.query.getOrElse("no_query"), //Option, so you'll need to do a get before accessing the CustomerFeedback case class member.
        "location" -> issue.zeroResults.location.getOrElse("no_location"),
        "count" -> issue.zeroResults.count.getOrElse(0),
        "percentViews" -> issue.zeroResults.percentViews.getOrElse(0.0)))
    issueCollection.save(newIssueDocument) // Have to loop through number of newIssue in CSV file.
    true
  }

  def saveUserFeedback(issue: Issue, issueCollection: MongoCollection): Boolean = {
    val newIssueDocument = MongoDBObject("_id" -> new ObjectId,
      "issueID" -> issue.issueID,
      "issueType" -> issue.issueType,
      "summary" -> issue.summary,
      "status" -> issue.status,
      "classification" -> issue.classification,
      "date" -> issue.date,
      "priority" -> issue.priority,
      "lastUpdated" -> new java.util.Date(),
      "crossReferenceID" -> issue.crossReferenceID,
      "attachments" -> issue.attachments,
      "action" -> issue.action,
      "userFeedback" -> MongoDBObject("query" -> issue.userFeedback.query.getOrElse("no-query"), //Option, so you'll need to do a get before accessing the CustomerFeedback case class member.
        "location" -> issue.userFeedback.location.getOrElse("no_location"),
        "count" -> issue.zeroResults.count.getOrElse(0),
        "reporter" -> issue.userFeedback.reporter,
        "problemSource" -> issue.userFeedback.problemSource,
        "endUserFeedback" -> issue.userFeedback.endUserFeedback,
        "searchEngineerFeedback" -> issue.userFeedback.searchEngineerFeedback,
        "npsScore" -> issue.userFeedback.npsScore.getOrElse(-1),
        "verbatimFeedback" -> issue.userFeedback.verbatimFeedback,
        "feedbackClass" -> issue.userFeedback.feedbackClass))
    issueCollection.save(newIssueDocument) // Have to loop through number of newIssue in CSV file.
    true
  }

  def saveNewIssue(issue: Issue): Boolean = {
    val issueCollection = MongoConnection()("sensis")("issues")
    def typeMatcher(issueType: String): Boolean = issue.issueType match {
      case "Customer-Feedback" => saveCustomerFeedback(issue, issueCollection)
      case "Zero-Result" => saveZeroFeedback(issue, issueCollection)
      case "User-Feedback" => saveUserFeedback(issue, issueCollection)
      case _ => false
    }
    typeMatcher(issue.issueType)
  }

  def updateIssue(issue: Issue): Boolean = {
    val issueCollection = MongoConnection()("sensis")("issues")
    val dbObject = MongoDBObject("issueID" -> issue.issueID)
    val typeCheckerChar = issue.issueID.charAt(0)
    def typeMatcher(typeCheckerChar: Char): Boolean = typeCheckerChar match {
      case 'Z' =>
        val update = $set("summary" -> issue.summary, "status" -> issue.status, "classification" -> issue.classification,
          "date" -> issue.date, "priority" -> issue.priority, "lastUpdated" -> issue.lastUpdated,
          "crossReferenceID" -> issue.crossReferenceID, "action" -> issue.action,
          "zeroResult.query" -> issue.zeroResults.query, "zeroResult.location" -> issue.zeroResults.location,
          "zeroResult.count" -> issue.zeroResults.count, "zeroResult.percentViews" -> issue.zeroResults.percentViews)

        issueCollection.update(dbObject, update)
        true
      case 'U' =>
        val update = $set("summary" -> issue.summary, "status" -> issue.status, "classification" -> issue.classification,
          "date" -> issue.date, "priority" -> issue.priority, "lastUpdated" -> issue.lastUpdated,
          "crossReferenceID" -> issue.crossReferenceID, "action" -> issue.action,
          "userFeedback.query" -> Option(issue.userFeedback.query), "userFeedback.location" -> issue.userFeedback.location,
          "userFeedback.count" -> issue.userFeedback.count, "userFeedback.reporter" -> issue.userFeedback.reporter,
          "userFeedback.problemSource" -> issue.userFeedback.problemSource, "userFeedback.endUserFeedback" -> issue.userFeedback.endUserFeedback,
          "userFeedback.searchEngineerFeedback" -> issue.userFeedback.searchEngineerFeedback, "userFeedback.npsScore" -> issue.userFeedback.npsScore,
          "userFeedback.verbatimFeedback" -> issue.userFeedback.verbatimFeedback, "userFeedback.feedbackClass" -> issue.userFeedback.feedbackClass)
        issueCollection.update(dbObject, update)
        true
      case 'C' =>
        val update = $set("summary" -> issue.summary, "status" -> issue.status, "classification" -> issue.classification,
          "date" -> issue.date, "priority" -> issue.priority, "lastUpdated" -> issue.lastUpdated,
          "crossReferenceID" -> issue.crossReferenceID, "action" -> issue.action,
          "customerFeedback.query" -> issue.customerFeedback.query, "customerFeedback.location" -> issue.customerFeedback.location, "customerFeedback.initialCommunication" -> issue.customerFeedback.initialCommunication,
          "customerFeedback.reporter" -> issue.customerFeedback.reporter, "customerFeedback.estimatedCost" -> issue.customerFeedback.estimatedCost,
          "customerFeedback.listingsEffected" -> issue.customerFeedback.listingsEffected, "customerFeedback.advertiserID" -> issue.customerFeedback.advertiserID,
          "customerFeedback.liaison.name" -> issue.customerFeedback.liaison.name, "customerFeedback.liaison.email" -> issue.customerFeedback.liaison.email,
          "customerFeedback.liaison.role" -> issue.customerFeedback.liaison.role, "customerFeedback.liaison.dept" -> issue.customerFeedback.liaison.dept)
        issueCollection.update(dbObject, update)
        true
    }
    typeMatcher(typeCheckerChar)
  }

  def saveUserFeedbackIssueToMongoDB(issue: Issue): Boolean = {
    val issueCollection = MongoConnection()("sensis")("issues")
    val newIssueDocument = MongoDBObject("_id" -> issue._id,
      "issueID" -> issue.issueID,
      "issueType" -> issue.issueType,
      "summary" -> "No Summary Exists.",
      "status" -> "open",
      "classification" -> issue.classification,
      "date" -> issue.date,
      "priority" -> "Un-Prioritized",
      "lastUpdated" -> issue.lastUpdated,
      "crossReferenceID" -> issue.crossReferenceID,
      "attachments" -> issue.attachments, // Doing Now..
      "action" -> issue.action,
      "userFeedback" -> MongoDBObject("query" -> Some(issue.userFeedback.query), //Option, so you'll need to do a get before accessing the CustomerFeedback case class member.
        "location" -> Some(issue.userFeedback.location),
        "count" -> Some(issue.userFeedback.count),
        "reporter" -> issue.userFeedback.reporter,
        "problemSource" -> issue.userFeedback.problemSource,
        "endUserFeedback" -> issue.userFeedback.endUserFeedback,
        "searchEngineerFeedback" -> issue.userFeedback.searchEngineerFeedback,
        "npsScore" -> issue.userFeedback.npsScore,
        "verbatimFeedback" -> issue.userFeedback.verbatimFeedback,
        "feedbackClass" -> issue.userFeedback.feedbackClass))
    issueCollection.save(newIssueDocument) // Have to loop through number of newIssue in CSV file.
    return true
  }

  def getIssue(issueID: String): Issue = {
    val retrieveDocument = MongoConnection()("sensis")("issues")
    val Id = MongoDBObject("issueID" -> issueID)
    var issue = retrieveDocument.find(Id).next
    issue.get("issueType") match {
      case "Customer-Feedback" => getCustomerFeedback(issue)
      case "Zero-Result" => getZeroFeedback(issue)
      case "User-Feedback" => getUserFeedback(issue)
    }
  }

  def retrieveAndSanitizeIssueWithSkip(skipNum: Int): List[Issue] = { // From controllers 
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    var issueCursor = retrieveCollection.find().limit(20).skip((skipNum - 1) * 20)
    while (issueCursor.hasNext) {
      var issue = issueCursor.next
      issue.get("issueType") match {
        case "Customer-Feedback" => issueList += getCustomerFeedback(issue)
        case "Zero-Result" => issueList += getZeroFeedback(issue)
        case "User-Feedback" => issueList += getUserFeedback(issue)
      }
    }
    return issueList.toList
  }

  def saveZeroResultsToMongoDB(zeroResults: ZeroResults) = {
    val ZeroResultsCollection = MongoConnection()("sensis")("issues")
   
    val autoIssueID = "ZFB" + "-" + getNextSequence.get
    val newIssueDocument = MongoDBObject("_id" -> new ObjectId,
      "issueID" -> autoIssueID,
      "issueType" -> "Zero-Result",
      "summary" -> "",
      "status" -> "open",
      "classification" -> List[String]("Un-Classified"),
      "date" -> new java.util.Date,
      "priority" -> "Un-Prioritized",
      "lastUpdated" -> new java.util.Date,
      "crossReferenceID" -> "No Reference yet.",
      "attachments" -> controllers.CreateIssue.saveAttachmentURL, // Doing Now..
      "action" -> "No Action Found.",
      "zeroResult" -> MongoDBObject("query" -> zeroResults.query, //Option, so you'll need to do a get before accessing the CustomerFeedback case class member.
        "location" -> zeroResults.location,
        "count" -> zeroResults.count,
        "percentViews" -> zeroResults.percentViews))
    ZeroResultsCollection.save(newIssueDocument)

  WS.url("http://localhost:9000/deploy/format/"+autoIssueID).get()

  }

  def priorityCount: List[Int] = {
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val issueList = retrieveCollection.find()
    var highcount = 0
    var lowCount = 0
    var medCount = 0
    var unpriorCount = 0
    while (issueList.hasNext) {
      var issue = issueList.next
      if (issue.get("priority").equals("high")) {
        highcount += 1
      } else if (issue.get("priority").equals("low")) {
        lowCount += 1
      } else if (issue.get("priority").equals("medium")) {
        medCount += 1
      } else unpriorCount += 1
    }
    val countList = List(highcount, lowCount, medCount, unpriorCount)
    countList
  }

  def searchIssue(location: String, searchFromDate: String, reporter: String, classification: String, source: String, fromDate: Option[String], toDate: Option[String], priority: Option[String], status: Option[String], issueType: Option[String]): List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val regExplocation = (""".*""" + location + """.*""").r
    val regExpSearchFromDate = (""".*""" + searchFromDate + """.*""").r
    val regExpReporter = (""".*""" + reporter + """.*""").r
    val parameters = MongoDBObject("userFeedback.location" -> regExplocation) ++ ("date" -> regExpSearchFromDate) ++ ("userFeedback.reporter" -> regExpReporter)
    var issueCursor = retrieveCollection.find(parameters)
    while (issueCursor.hasNext) {
      var issue = issueCursor.next
      issue.get("issueType") match {
        case "Customer-Feedback" => issueList += getCustomerFeedback(issue)
        case "Zero-Result" => issueList += getZeroFeedback(issue)
        case "User-Feedback" => issueList += getUserFeedback(issue)
      }
    }
    return issueList.toList
  }

  def simpleSearch(param: String): List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val parameters = $or("userFeedback.query" -> param, "userFeedback.reporter" -> param, "zeroResult.query" -> param, "customerFeedback.query" -> param)
    val list1 = retrieveCollection.db.command(MongoDBObject("text" -> "issues", "search" -> param)).getAs[MongoDBList]("results").iterator
    while (list1.hasNext) {
      val result = list1.next.asInstanceOf[MongoDBList]
      val second = result.iterator()
      while (second.hasNext) {
        val issue = second.next().asInstanceOf[BasicDBObject].get("obj").asInstanceOf[BasicDBObject]
        issue.get("issueType") match {
          case "Customer-Feedback" => issueList += getCustomerFeedback(issue)
          case "Zero-Result" => issueList += getZeroFeedback(issue)
          case "User-Feedback" => issueList += getUserFeedback(issue)
        }
      }
    }
    return issueList.toList
  }

  def getCustomerFeedback(issue: DBObject): Issue = {
    val attachmentList = getAttachments(issue)
    val dbObject = issue.get("customerFeedback").asInstanceOf[DBObject]
    val dbObjectLiaison = dbObject.get("liaison").asInstanceOf[DBObject]
    val listToStringCommunication = getInitialCommunication(dbObject)
    new Issue(issue.get("_id").asInstanceOf[ObjectId],
      issue.getAs[String]("issueID").get,
      issue.getAs[String]("issueType").get,
      issue.getAs[String]("summary").get,
      issue.getAs[String]("status").get,
      getClassification(issue),
      issue.get("date").asInstanceOf[java.util.Date],
      issue.getAs[String]("priority").get,
      issue.get("lastUpdated").asInstanceOf[java.util.Date],
      issue.getAs[String]("crossReferenceID").get,
      attachmentList,
      issue.getAs[String]("action").get,
      issue.get("zeroResults").asInstanceOf[ZeroResults],
      issue.get("userFeedback").asInstanceOf[UserFeedback],
      new CustomerFeedback(dbObject.getAs[String]("query"),
        dbObject.getAs[String]("location"),
        dbObject.getAs[String]("reporter"),
        Some(listToStringCommunication),
        dbObject.getAs[String]("estimatedCost"),
        dbObject.getAs[String]("listingsEffected"),
        dbObject.getAs[String]("advertiserID"),
        new Liaison(dbObjectLiaison.getAs[String]("name"),
          dbObjectLiaison.getAs[String]("email"),
          dbObjectLiaison.getAs[String]("role"),
          dbObjectLiaison.getAs[String]("dept"))))
  }

  def getUserFeedback(issue: DBObject) = {
    val attachmentList = getAttachments(issue)
    val dbObject = issue.get("userFeedback").asInstanceOf[DBObject]
    countIssuesByReporter
    new Issue(issue.get("_id").asInstanceOf[ObjectId],
      issue.getAs[String]("issueID").get,
      issue.getAs[String]("issueType").get,
      issue.getAs[String]("summary").get,
      issue.getAs[String]("status").get,
      getClassification(issue),
      issue.get("date").asInstanceOf[java.util.Date],
      issue.getAs[String]("priority").get,
      issue.get("lastUpdated").asInstanceOf[java.util.Date],
      issue.getAs[String]("crossReferenceID").get,
      attachmentList,
      issue.getAs[String]("action").get,
      issue.get("zeroResults").asInstanceOf[ZeroResults],
      new UserFeedback(dbObject.getAs[String]("query"),
        dbObject.getAs[String]("location"),
        dbObject.getAs[Int]("count"),
        dbObject.getAs[String]("reporter"),
        dbObject.getAs[String]("problemSource"),
        dbObject.getAs[String]("endUserFeedback"),
        dbObject.getAs[String]("searchEngineerFeedback"),
        dbObject.getAs[Int]("npsScore"),
        dbObject.getAs[String]("verbatimFeedback"),
        dbObject.getAs[String]("feedbackClass")),
      issue.get("customerFeedback").asInstanceOf[CustomerFeedback])
    
  }

  def getZeroFeedback(issue: DBObject) = {

    val attachmentList = getAttachments(issue)
    val dbObject = issue.get("zeroResult").asInstanceOf[DBObject]
    new Issue(issue.get("_id").asInstanceOf[ObjectId],
      issue.getAs[String]("issueID").get,
      issue.getAs[String]("issueType").get,
      issue.getAs[String]("summary").get,
      issue.getAs[String]("status").get,
      getClassification(issue),
      issue.get("date").asInstanceOf[java.util.Date],
      issue.getAs[String]("priority").get,
      issue.get("lastUpdated").asInstanceOf[java.util.Date],
      issue.getAs[String]("crossReferenceID").get,
      attachmentList,
      issue.getAs[String]("action").get,
      new ZeroResults(dbObject.getAs[String]("query"),
        dbObject.getAs[String]("location"),
        dbObject.getAs[Int]("count"),
        dbObject.getAs[Double]("percentViews")),
      issue.get("userFeedback").asInstanceOf[UserFeedback],
      issue.get("customerFeedback").asInstanceOf[CustomerFeedback])

  }

  def getClassification(issue: DBObject): List[String] = {
    val listAttachment: BasicDBList = issue.get("classification").asInstanceOf[BasicDBList]
    val listToStringAttachment = new ArrayBuffer[String]
    for (i <- 0 to listAttachment.length - 1) {
      val z = listAttachment.get(i)
    //  println("printed here: " + z)
      listToStringAttachment += z.toString()
    }
    listToStringAttachment.toList
  }

  def getAttachments(issue: DBObject): List[String] = {
    val listAttachment: BasicDBList = issue.get("attachments").asInstanceOf[BasicDBList]
    val listToStringAttachment = new ArrayBuffer[String]
    for (i <- 0 to listAttachment.length - 1) {
      val z = listAttachment.get(i)
    //  println("printed here: " + z)
      listToStringAttachment += z.toString()
    }
    listToStringAttachment.toList
  }

  def getInitialCommunication(dbObject: DBObject): List[String] = {
    val listCommunication: BasicDBList = dbObject.get("initialCommunication").asInstanceOf[BasicDBList]
    val listToStringCommucation = new ArrayBuffer[String]
    for (i <- 0 to listCommunication.length - 1) {
      val z = listCommunication.get(i)
     // println("printed here: " + z)
      listToStringCommucation += z.toString()
    }
    listToStringCommucation.toList
  }
  def numOfDocumentsInDB: Double = {
    val retrieveCollection = MongoConnection()("sensis")("issues")
//    println((retrieveCollection.size.toDouble / 20).ceil)
    (retrieveCollection.size.toDouble / 20).ceil
  }

  def updateAttachment(issueID: String, saveAttachmentURL: List[String]) = {
//    println(saveAttachmentURL)
    val collection = MongoConnection()("sensis")("issues")
    val dbObject = MongoDBObject("issueID" -> issueID)
    saveAttachmentURL.map { attachment =>
      val update = $push("attachments" -> attachment)
      collection.update(dbObject, update)
    }
  }
  def issueTypeCount: List[Int] = {

    val collection = MongoConnection()("sensis")("issues")

    val dbObjectZero = MongoDBObject("issueType" -> "Zero-Result")
    val zeroResultCount = collection.find(dbObjectZero).count

    val dbObjectUser = MongoDBObject("issueType" -> "User-Feedback")
    val userFeedbackCount = collection.find(dbObjectUser).count

    val dbObjectCustomer = MongoDBObject("issueType" -> "Customer-Feedback")
    val customerFeedBack = collection.find(dbObjectCustomer).count

    List(zeroResultCount, userFeedbackCount, customerFeedBack)
  }
  def fetchZeroIssues: List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val dbObjectZero = MongoDBObject("issueType" -> "Zero-Result")
    var issueCursor = retrieveCollection.find(dbObjectZero)
    while (issueCursor.hasNext) {
      issueList += getZeroFeedback(issueCursor.next)
    }
    return issueList.toList
  }
  
  
  def fetchUserIssues: List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val dbObjectZero = MongoDBObject("issueType" -> "User-Feedback")
    var issueCursor = retrieveCollection.find(dbObjectZero)
    while (issueCursor.hasNext) {
      issueList += getUserFeedback(issueCursor.next)
    }
    return issueList.toList
  }

  def fetchCustIssues: List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val dbObjectZero = MongoDBObject("issueType" -> "Customer-Feedback")
    var issueCursor = retrieveCollection.find(dbObjectZero)
    while (issueCursor.hasNext) {
      issueList += getCustomerFeedback(issueCursor.next)
    }
    return issueList.toList
  }
  def countIssueByStatus(status: String): Int = {
    MongoConnection()("sensis")("issues").find(MongoDBObject("status" -> status)).count
  }
  def issueStatusCount: List[Int] = {
    List(countIssueByStatus("open"),
      countIssueByStatus("resolved"),
      countIssueByStatus("new"),
      countIssueByStatus("inProgress"),
      countIssueByStatus("notAnIssue"))
  }
  def countIssuePerMonth(month: String) = {
    MongoConnection()("sensis")("issues").find(MongoDBObject("date" -> month)).count
  }
  def deleteIssue(issueID: String): Boolean = {
//    println(issueID)
    MongoConnection()("sensis")("issues").remove(MongoDBObject("issueID" -> issueID))
//    println("executed delete")
    true
  }

  def countIssuesByReporter: List[Int] = {
    val formatDate = new java.text.SimpleDateFormat("dd/MM/yy")
    
    val g = MongoDBObject("date"-> MongoDBObject("$lt" -> formatDate.parse("01/12/13"), "$gt" ->formatDate.parse("01/11/13") ))// , "$group"-> MongoDBObject("_id"->"$userFeedback.reporter", "Aug"-> MongoDBObject("$sum" ->1)))
    val x = MongoConnection()("sensis")("issues").find(g)
    // x.foreach(f => println("here "+ f))
   // println(MongoConnection()("sensis")("issues").aggregate(g).results.toList )
    
    val countOne = MongoConnection()("sensis")("issues").find(MongoDBObject("customerFeedback.reporter" -> "Willy")).count + MongoConnection()("sensis")("issues").find(MongoDBObject("userFeedback.reporter" -> "Willy")).count
    val countTwo = MongoConnection()("sensis")("issues").find(MongoDBObject("customerFeedback.reporter" -> "Karl")).count + MongoConnection()("sensis")("issues").find(MongoDBObject("userFeedback.reporter" -> "Karl")).count
    List(countOne, countTwo)
  }
  
}
