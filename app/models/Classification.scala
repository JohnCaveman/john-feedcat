package models

import play.api.Play.current
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.MongoConnection
import com.mongodb.casbah.commons.MongoDBObject
import play.api.libs.json._
import scala.collection.mutable.ArrayBuffer

case class Classification(classificationName: String, subClassification: List[String])

case class Category(superClassification: String, subClassification: String)

case class SubClassification(value: List[String])

object Classification {

  def retrieveClassifications(): List[Classification] = {

    var classificationList = new ArrayBuffer[Classification]
    val retrieveCollection = MongoConnection()("sensis")("classification")

    val classificationCursor = retrieveCollection.find()

    while (classificationCursor.hasNext) {
      var subClassificationList = new ArrayBuffer[String]
      val classification = classificationCursor.next
      val x: BasicDBList = classification.get("Sub-Classification").asInstanceOf[BasicDBList]
      for (i <- 0 to x.length - 1) {
        val z = x.get(i).asInstanceOf[BasicDBObject]
        subClassificationList += z.get("value").toString()
      }
      classificationList += (new Classification(classification.get("Classification").toString(), subClassificationList.toList))
    }

    classificationList.toList
  }

  def saveClassification(category: Category): Boolean = {
    val retrieveCollection = MongoConnection()("sensis")("classification")
    retrieveCollection.update(MongoDBObject("Classification" -> category.superClassification), $addToSet("Sub-Classification" -> MongoDBObject("value" -> category.subClassification.trim())))
    true

  }

  def deleteClassification(classList: SubClassification): Boolean = {

    val retrieveCollection = MongoConnection()("sensis")("issues")
    for (value <- classList.value) {
      val a = value.split("-")
      while (retrieveCollection.find(MongoDBObject("classification" -> value)).hasNext) {
        retrieveCollection.find(MongoDBObject("classification" -> value)).next.get("classification").asInstanceOf[BasicDBList].length match {
          case 1 => {
            retrieveCollection.update(MongoDBObject("classification" -> value), $pull("classification" -> value), false, false)
            retrieveCollection.update(MongoDBObject("classification" -> value), $push("classification" -> "Un-Classified"), false, false)
          }
          case _ => retrieveCollection.update(MongoDBObject("classification" -> value), $pull("classification" -> value), false, false)
        }
      }
//      println(a(1).trim)
      val conn = MongoConnection()("sensis")("classification")
      conn.update(MongoDBObject("Classification" -> a(0).trim), $pull("Sub-Classification" -> MongoDBObject("value" -> a(1).trim)), false, true)
    }
    return true
  }

}