package models

import anorm._
import anorm.SqlParser._
import play.api.Play.current
import play.api.db._

case class UserRegister(firstName: String, lastName: String, username: String, password: String, userType: String)

object UserRegister {

  val simple = {
    get[String]("user.firstName") ~
      get[String]("user.lastName") ~
      get[String]("user.username") ~
      get[String]("user.password") ~
      get[String]("user.userType") map {
        case firstName ~ lastName ~ username ~ password ~ userType => UserRegister(firstName, lastName, username, password, userType)
      }

  }

  def create(firstName: String, lastName: String, username: String, password: String, userType: String) = {
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
          insert into user(User_Name , Password , User_Type , First_Name , Last_Name) values (
            {username}, {password}, {userType}, {firstName}, {lastName}
          )
        """).on(
          'firstName -> firstName,
          'lastName -> lastName,
          'username -> username,
          'password -> password,
          'userType -> userType).execute() //   task.copy(id = Id(id))
    }
    true
  }

 

}

