package models

import play.api.Play.current
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoConnection._
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.MongoDB
import scala.io.Source
import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._
import scala.collection.mutable.ArrayBuffer

case class Search(query: String,
  location: String,
  date: String,
  reporter: String,
  classification: List[String],
  priority: Option[String],
  status: Option[String],
  issueType: Option[String])

case class SimpleSearch(param: String)

object Search {
  
  
  
  
  

  def searchIssue(query: String, location: String, date: String, reporter: String, classification: List[String], priority: Option[String], status: Option[String], issueType: Option[String]): List[Issue] = {
    var issueList = new ArrayBuffer[Issue]
    val retrieveCollection = MongoConnection()("sensis")("issues")
    

       
   val advancedSearchList : Option[MongoDBList]=  (date, priority, status, issueType) match {
       	
       case (date,Some("all"), Some("all"), Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " " + date))).getAs[MongoDBList]("results")
       
       case (date, Some("all"), _ , Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ")), "filter" -> MongoDBObject("status"->status))).getAs[MongoDBList]("results")
       
       case (date, _, Some("all") , Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ date), "filter" -> MongoDBObject("priority"->priority))).getAs[MongoDBList]("results")
       
       case (date, Some("all"), Some("all"), _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ date), "filter" -> MongoDBObject("issueType"->issueType))).getAs[MongoDBList]("results")
       
     //   case (_,Some("all"), Some("all"), Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ")))).getAs[MongoDBList]("results")
       
//       case (""|null, Some("all"), _ , Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ status))).getAs[MongoDBList]("results")
//       
//       case (""|null, _, Some("all") , Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ priority))).getAs[MongoDBList]("results")
//       
//       case (""|null, Some("all"), Some("all"), _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ issueType))).getAs[MongoDBList]("results")
//       
//       case (""|null, Some("all"), _, _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ status + " " + issueType))).getAs[MongoDBList]("results")
//       
//       
       case (date, Some("all"), _, _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " " + date), "filter" -> MongoDBObject("status"->status , "issueType"->issueType))).getAs[MongoDBList]("results")
       
       case (date, _, Some("all"), _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " " + date), "filter" -> MongoDBObject("priority"->priority , "issueType"->issueType))).getAs[MongoDBList]("results")
      
       case (date, _, _, Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ")+ " " + date), "filter" -> MongoDBObject("priority"->priority , "status"->status))).getAs[MongoDBList]("results")
       
       case (date, _, _, _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") +" " + date), "filter" -> MongoDBObject("priority"->priority , "issueType"->issueType, "status" -> status))).getAs[MongoDBList]("results")
       
       
      
//       case (""|null, _, Some("all"), _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ priority + " " + issueType))).getAs[MongoDBList]("results")
//      
//       case (""|null, _, _, Some("all")) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ status + " " + priority))).getAs[MongoDBList]("results")
//       
//       case (_, _, _, _) => retrieveCollection.db.command(MongoDBObject("text" -> "issues",  "search" -> (query +" "+ location + " " + reporter + " "+ classification.mkString(" ") + " "+ status + " " + issueType+ " " + priority))).getAs[MongoDBList]("results")
       
          
     
      }
    
      
      
   
    
//    println(advancedSearchList)
    
    val advancedSearchIterator = advancedSearchList.iterator

    while (advancedSearchIterator.hasNext) {
      val result = advancedSearchIterator.next.asInstanceOf[MongoDBList]

      val second = result.iterator()
      while (second.hasNext) {
        //println(second.next().asInstanceOf[BasicDBObject].get("obj").asInstanceOf[BasicDBObject].get("_id"))

        val issue = second.next().asInstanceOf[BasicDBObject].get("obj").asInstanceOf[BasicDBObject]
        issue.get("issueType") match {
          case "Customer-Feedback" => issueList += Issue.getCustomerFeedback(issue)
          case "Zero-Result" => issueList += Issue.getZeroFeedback(issue)
          case "User-Feedback" => issueList += Issue.getUserFeedback(issue)

        }

      }
    }

  
    return issueList.toList
  }

}