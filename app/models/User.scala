package models

import scala.io.Source
import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._
import scala.collection.mutable.ArrayBuffer

case class UserEntity(firstName: String,userID : String)
case class User(userID: String, pass: String, userType: String)
case class UserForSession(userID: String, pass: String, userType: String, firstName: String, lastName: String)
case class UserToDel(userID: String)



case class Change(username: String , originalPass: String , changedPass : String)

object User {
  
  

  val simple = {
    get[String]("user.User_Name") ~
      get[String]("user.Password") ~
      get[String]("user.User_Type") map {
        case userID ~ pass ~ userType => User(userID, pass, userType)
      }
  }

  val simpleUser = {
    get[String]("user.First_Name")~
    get[String]("user.User_Name") map {
      case firstName~userID => UserEntity(firstName,userID)
    }
  }

  val simpleUserForSession = {
    get[String]("user.User_Name") ~
      get[String]("user.Password") ~
      get[String]("user.User_Type") ~
      get[String]("user.First_Name") ~
      get[String]("user.Last_Name") map {
        case userID ~ pass ~ userType ~ firstName ~ lastName => UserForSession(userID, pass, userType, firstName, lastName)
      }
  }
  val db = DB.getDataSource("sensis")

  def authenticate(u: String, z: String): Option[User] = {
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
         select User_Name, Password, User_Type from user where 
         User_Name = {abc} and Password = {p}
        """).on(
          'abc -> u,
          'p -> z).as(User.simple.singleOpt)
    }
  }
  
  
  
  def changePassword(userName : String, newPassword: String, oldPassword : String ): Boolean = {
    
    
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
         update User SET Password = {npass} where 
         User_Name = {uname} and Password ={opass}
        """).on(
          'uname -> userName,
          'npass -> newPassword,
          'opass -> oldPassword).execute()
    }
    true
  }

  def getUserType(u: User): User = {
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
         select User_Name, Password, User_Type from user where 
         User_Name = {abc}
      """).on(
          'abc -> u.userID).as(User.simple.single)
    }
  }

  def getUser(u: User): UserForSession = {
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
         select User_Name, Password, User_Type, First_Name,Last_Name from user where 
         User_Name = {abc}
      """).on(
          'abc -> u.userID).as(User.simpleUserForSession.single)
    }
  }

  def getAllUsers(): List[UserEntity] = {
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
         select First_Name, User_Name from user
      """).as(User.simpleUser *)
    }

  }

  def delete(userID: String): Boolean = {
    println(userID)
    DB.withConnection("sensis") { implicit connection =>
      SQL(
        """
            DELETE FROM user WHERE User_Name={userID}
           
        """).on(
          'userID -> userID).execute() //   task.copy(id = Id(id))

    }
    true
  }

  //As is often the case with methods suffixed with "opt", it means that it returns an Option. So the difference between those 2 methods is that singlewill treat the absence of result as an error, while singleOpt will just return None when there is no result (and the actual result wrapped in Some otherwise).

  // abc is the dynamic parameter passed in query 'abc is mapped to "u"  passed to function

  ///get[String]("User_Name") ~ get[String]("User_Type") *
}