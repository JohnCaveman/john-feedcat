package controllers

import java.io.File
import java.io.FileReader
import scala.collection.JavaConversions._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.Imports.MongoDBObject
import au.com.bytecode.opencsv.CSVReader
import models._
import play.Play
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import play.api.mvc._
import views._
import java.util.Calendar
import scala.util.Random
import scalax.io._
import scala.io.Source
import java.util.Date
import play.api.libs.Files
import play.api.data.Form
import play.api.data.format.Formats._
import org.joda.time
import org.joda.time.convert
import org.joda.time.format
import play.api.libs.ws.WS

object CreateIssue extends Controller {

  val filePath = "/conf/data/"

  val filePathAttachment = "/conf/attachments/"

  //Handling default requests. to load product form
  def create = Action { implicit request =>
    Ok(views.html.createIssue("Hello"))
  }

  val cal = Calendar.getInstance();
  val day = cal.get(Calendar.DAY_OF_MONTH);
  val month = cal.get(Calendar.MONTH)
  val year = cal.get(Calendar.YEAR)

  val formatDateUpload = new java.text.SimpleDateFormat("dd/MM/yy")
  val formatDate = new java.text.SimpleDateFormat("dd/MM/yyyy")
  val formatDateUpdate = new java.text.SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy")

  def createIssueID(issueType: String): String = issueType match { 

    case "User-Feedback" => "UFB" + "-" + Issue.getNextSequence.get
    case "Customer-Feedback" => "CFB" + "-" + Issue.getNextSequence.get
    case "Zero-Result" => "ZFB" + "-" +Issue.getNextSequence.get

  }

  def updateIssue = Action {
    implicit request =>
      def writeFile(file: MultipartFormData.FilePart[Files.TemporaryFile], issueID: String): String = {
        val filename = file.filename
        val contentType = file.contentType
        saveAttachmentURL ::= filename.toString()
        //println(saveAttachmentURL)
        file.ref.moveTo(new File(Play.application.path +
          filePathAttachment + issueID + "/" + filename), true)
        filename
      }

      updateIssueForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.createNewIssue(formWithErrors, Classification.retrieveClassifications)),
        issue => request.body.asMultipartFormData.headOption.map { file =>

          for (individualFile <- file.files) yield writeFile(individualFile, issue.issueID) // Should generate Issue ID here to pass variable to attachment Folder.    
          Issue.updateAttachment(issue.issueID, saveAttachmentURL)
          Redirect(routes.Application.issues)

        }.getOrElse {
          Ok(views.html.success("Bad."))
        })
  }

  val updateIssueForm: Form[Issue] = Form(
    mapping(
      "issueID" -> text,
      "issueSummary" -> text,
      "issueStatus" -> text,
      "classification" -> list(text),
      "date" -> text,
      "issuePriority" -> text,
      "crossReferenceID" -> text,
      "lastUpdated" -> text,
      "action" -> text,
      "zeroResults" -> mapping(
        "query" -> optional(text),
        "location" -> optional(text),
        "count" -> optional(number),
        "percentage" -> optional(of[Double]))(ZeroResults.apply)(ZeroResults.unapply), // OfDouble Nice Explaination http://stackoverflow.com/questions/12759430/play-bind-a-form-field-to-a-double
      "userFeedback" -> mapping(
        "query2" -> optional(text),
        "location2" -> optional(text),
        "count2" -> optional(number),
        "reporter" -> optional(text),
        "problemSource" -> optional(text),
        "endUserFeedback" -> optional(text),
        "searchEngineerFeedback" -> optional(text),
        "npsScore" -> optional(number),
        "verbatimFeedback" -> optional(text),
        "feedbackClass" -> optional(text))(UserFeedback.apply)(UserFeedback.unapply),
      "customerFeedback" -> mapping(
        "query3" -> optional(text),
        "location3" -> optional(text),
        "reporter" -> optional(text),
        "initialCommunication" -> optional(list(text)),
        "estimatedCost" -> optional(text),
        "listingsEffected" -> optional(text),
        "advertiserID" -> optional(text),
        "liaison" -> mapping(
          "name" -> optional(text),
          "email" -> optional(text),
          "role" -> optional(text),
          "dept" -> optional(text))(Liaison.apply)(Liaison.unapply))(CustomerFeedback.apply)(CustomerFeedback.unapply)) { (issueID, issueSummary, issueStatus, classification, date, issuePriority, crossReferenceID, lastUpdated, action, zeroResults, userFeedback, customerFeedback) => Issue(null, issueID, null, issueSummary, issueStatus, classification, formatDateUpdate.parse(date), issuePriority, new Date(), crossReferenceID, saveAttachmentURL, action, zeroResults, userFeedback, customerFeedback) } { (issue: Issue) => Some(issue.issueID, issue.summary, issue.status, issue.classification, issue.date.toString(), issue.priority, issue.crossReferenceID, issue.lastUpdated.toString(), issue.action, issue.zeroResults, issue.userFeedback, issue.customerFeedback) }.verifying("Error in Inserting", issue => Issue.updateIssue(issue)))

  //Changed - To be updated in Prateek Code/.
  val newIssueForm: Form[Issue] = Form(
    mapping(
      // "issueID" -> text,
      "issueType" -> text,
      "issueSummary" -> text,
      "issueStatus" -> text,
      "classification" -> list(text),
      "date" -> text,
      "issuePriority" -> text,
      "crossReferenceID" -> text,
      "action" -> text,
      "zeroResults" -> mapping(
        "query" -> optional(text),
        "location" -> optional(text),
        "count" -> optional(number),
        "percentage" -> optional(of[Double]))(ZeroResults.apply)(ZeroResults.unapply), // OfDouble Nice Explaination http://stackoverflow.com/questions/12759430/play-bind-a-form-field-to-a-double
      "userFeedback" -> mapping(
        "query2" -> optional(text),
        "location2" -> optional(text),
        "count2" -> optional(number),
        "reporter" -> optional(text),
        "problemSource" -> optional(text),
        "endUserFeedback" -> optional(text),
        "searchEngineerFeedback" -> optional(text),
        "npsScore" -> optional(number),
        "verbatimFeedback" -> optional(text),
        "feedbackClass" -> optional(text))(UserFeedback.apply)(UserFeedback.unapply),
      "customerFeedback" -> mapping(
        "query3" -> optional(text),
        "location3" -> optional(text),
        "reporter2" -> optional(text),
        "initialCommunication" -> optional(list(text)),
        "estimatedCost" -> optional(text),
        "listingsEffected" -> optional(text),
        "advertiserID" -> optional(text),
        "liaison" -> mapping(
          "name" -> optional(text),
          "email" -> optional(text),
          "role" -> optional(text),
          "dept" -> optional(text))(Liaison.apply)(Liaison.unapply))(CustomerFeedback.apply)(CustomerFeedback.unapply)) { (issueType, issueSummary, issueStatus, classification, date, issuePriority, crossReferenceID, action, zeroResults, userFeedback, customerFeedback) => Issue(new ObjectId, createIssueID(issueType: String), issueType, issueSummary, issueStatus, classification, formatDate.parse(date), issuePriority, new Date(), crossReferenceID, saveAttachmentURL, action, zeroResults, userFeedback, customerFeedback) } { (issue: Issue) => Some(issue.issueType, issue.summary, issue.status, issue.classification, issue.date.toString(), issue.priority, issue.crossReferenceID, issue.action, issue.zeroResults, issue.userFeedback, issue.customerFeedback) }.verifying("Error in Inserting", issue => Issue.saveNewIssue(issue)))

  val submitIssueForm: Form[Issue] = Form(
    mapping(
      // "issueID" -> text,
      "issueType" -> text,
      //  "issueSummary" -> text,
      "issueStatus" -> text,
      //"classification" -> text,
      "issuePriority" -> text,
     
      "customerFeedback" -> mapping(
        "query3" -> optional(text),
        "location3" -> optional(text),
        "reporter2" -> optional(text),
        "initialCommunication" -> optional(list(text)),
        "estimatedCost" -> optional(text),
        "listingsEffected" -> optional(text),
        "advertiserID" -> optional(text),
        "liaison" -> mapping(
          "name" -> optional(text),
          "email" -> optional(text),
          "role" -> optional(text),
          "dept" -> optional(text))(Liaison.apply)(Liaison.unapply))(CustomerFeedback.apply)(CustomerFeedback.unapply)) { (issueType, issueStatus, issuePriority, customerFeedback) =>
        Issue(new ObjectId,
          createIssueID(issueType: String),
          issueType,
          " ",
          issueStatus,
          "Un-Classified" :: Nil,
          new Date(),
          issuePriority,
          new Date(),
          " ",
          saveAttachmentURL,
          " ",
          null,
          null,
          customerFeedback)
      } { issue =>
        Some(issue.issueType,
          issue.status,
          issue.priority,
          issue.customerFeedback)
      }.verifying("Error in Inserting", issue => Issue.saveNewIssue(issue)))

 

  def newIssue = Action { implicit request =>
    Ok(views.html.createNewIssue(newIssueForm, Classification.retrieveClassifications))
  }

  def zeroResults = Action { implicit request =>
    Ok(views.html.createZeroIssue("Hello"))
  }

  //  def createNewIssue = Action { implicit request =>
  //    
  //  }

  var saveAttachmentURL = List[String]()
  //(parse.multipartFormData)

  def createSubmitIssue = Action { implicit request =>
     def writeFile(file: MultipartFormData.FilePart[Files.TemporaryFile], issueID: String): String = {
        val filename = file.filename
        val contentType = file.contentType
        saveAttachmentURL ::= filename.toString()
//        println(saveAttachmentURL)
        file.ref.moveTo(new File(Play.application.path +
          filePathAttachment + issueID + "\\" + filename), true)
        filename
      }
      submitIssueForm.bindFromRequest.fold(
        formWithErrors =>BadRequest(views.html.anonIssueSubmit(formWithErrors)),
        issue => request.body.asMultipartFormData.headOption.map { file =>

          for (individualFile <- file.files) yield writeFile(individualFile, issue.issueID) // Should generate Issue ID here to pass variable to attachment Folder.    
          Issue.updateAttachment(issue.issueID, saveAttachmentURL)
           WS.url("http://localhost:9000/deploy/format/"+issue.issueID).get()
            Ok(views.html.success(issue.issueID))

        }.getOrElse {
          Ok(views.html.success("Bad."))
        })

//    submitIssueForm.bindFromRequest.fold(
//      formWithErrors => BadRequest(views.html.anonIssueSubmit(formWithErrors)),
//      issue => Ok(views.html.success(issue.issueID)))
  }

  def createNewIssue = Action {
    implicit request =>
      def writeFile(file: MultipartFormData.FilePart[Files.TemporaryFile], issueID: String): String = {
        val filename = file.filename
        val contentType = file.contentType
        saveAttachmentURL ::= filename.toString()
//        println(saveAttachmentURL)
        file.ref.moveTo(new File(Play.application.path +
          filePathAttachment + issueID + "\\" + filename), true)
        filename
      }

      newIssueForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.createNewIssue(formWithErrors, Classification.retrieveClassifications)),
        issue => request.body.asMultipartFormData.headOption.map { file =>

          for (individualFile <- file.files) yield writeFile(individualFile, issue.issueID) // Should generate Issue ID here to pass variable to attachment Folder.    
          Issue.updateAttachment(issue.issueID, saveAttachmentURL)
       WS.url("http://localhost:9000/deploy/format/"+issue.issueID).get()
          Redirect(routes.Application.issues)

        }.getOrElse {
          Ok(views.html.success("Bad."))
        })
  }

  def download(filename: String, issueID: String) = Action {
    val fullFilename = Play.application.path + filePathAttachment + issueID + "\\" + filename
    Ok.sendFile(new java.io.File(fullFilename))
  }

 def upload = Action(parse.multipartFormData) { request =>
    request.body.file("fileupload").map { file =>
      val filename = file.filename
      val contentType = file.contentType

      //moving csv file to application
      file.ref.moveTo(new File(Play.application.path +
        filePath + filename), true)

      //uploading products
      uploadUserFeedback(filename)

      //send message
      Redirect(controllers.routes.CreateIssue.create).
        flashing("message" -> "Issues uploaded successfully.")
    }.getOrElse {
      //send error message
      Redirect(controllers.routes.CreateIssue.create).
        flashing("errormessage" -> "File Missing/Wrong Format")
    }
  }

  def uploadUserFeedback(fileName: String) = {
    //Inserting products to the DB using specified CSV
    val issueReader = new CSVReader(new FileReader(Play.application.
      path + filePath + fileName))
    
    var x = 0;
   
    val rows = issueReader.readAll()
    for (row <- rows) {
      
      if (row(0) != "" && x != 0) {
        
        if (row.size > 1) {
          val issueID = createIssueID("User-Feedback")

          Issue.saveUserFeedbackIssueToMongoDB(new Issue(
            new ObjectId(),
            issueID,
            "User-Feedback",
            "No Summary Exists.",
            "open",
            List[String]("Un-Classified"),
            formatDateUpload.parse(row(3)).asInstanceOf[java.util.Date],
            "Un-Prioritized",
            new Date(),
            "No Reference",
            saveAttachmentURL,
            "No Action Found",
            null,
            UserFeedback(
                row(0) match { case ""|null => Some("no_query"); case _ => Some(row(0))},
                row(1) match { case ""|null => Some("no_location"); case _ => Some(row(1))},
                row(2) match { case ""|null => Some(0); case _ => Some(row(2).toInt)},
                row(4) match { case ""|null => Some("no_reporter"); case _ => Some(row(4))},
                row(5) match { case ""|null => Some("None"); case _ => Some(row(5))},
                row(8) match { case ""|null => Some("None"); case _ => Some(row(8))},
                row(9) match { case ""|null => Some("None"); case _ => Some(row(9))},
                row(6) match { case ""|null => Some(-1); case _ => Some(row(6).toInt)},
                Some("None"),
                row(7) match { case ""|null => Some("None"); case _ => Some(row(7))}), 
            null))
            
           WS.url("http://localhost:9000/deploy/format/"+issueID).get()
        }
      }
      x = x + 1
    }
  }
  //uploading FeedCAT zero results
  def uploadZero = Action(parse.multipartFormData) { request =>
    request.body.file("zerofileupload").map { zerofileupload =>
      val filename = zerofileupload.filename
      val contentType = zerofileupload.contentType

      //moving csv file to application
      zerofileupload.ref.moveTo(new File(Play.application.path +
        filePath + filename), true)

      //uploading products
      uploadZeroResults(filename)

      //send message
      Redirect(controllers.routes.CreateIssue.zeroResults).
        flashing("message" -> "Products uploaded successfully !!!")
    }.getOrElse {
      //send error message
      Redirect(controllers.routes.CreateIssue.zeroResults).
        flashing("errormessage" -> "File Missing")
    }
  }

  //FeedCAT Zero sample 
  def uploadZeroResults(fileName: String) = {
    Source.fromFile(Play.application.path + filePath + fileName).getLines.drop(24).foreach(line => processLine(line))
  }

  def processLine(line: String) = {
    //  println(line + "!!!!!!!!!!!!!!!")
    val lineContent = line.split(",")

    // lineContent.foreach(c => println(c))
    if (lineContent.length == 5) {
      val queryLoc = lineContent(1).split("_")

      if (queryLoc.length > 1) {
        Issue.saveZeroResultsToMongoDB(new ZeroResults(
          Some(queryLoc(0)),
          Some(queryLoc(1)),
          Some(lineContent(3).toInt),
          Some(lineContent(4).replace("%", "").toDouble)))

      } else {

        Issue.saveZeroResultsToMongoDB(new ZeroResults(
          Some(queryLoc(0)),
          Some("All Sates"),
          Some(lineContent(3).toInt),
          Some(lineContent(4).replace("%", "").toDouble)))
      }

    } else if (lineContent.length == 6) {
      val queryLoc = lineContent(1).split("_")

      if (queryLoc.length > 1) {

        Issue.saveZeroResultsToMongoDB(new ZeroResults(
          Some(queryLoc(0)),
          Some(queryLoc(1) + lineContent(2)),
          Some(lineContent(4).toInt),
          Some(lineContent(5).replace("%", "").toDouble)))

      } else {

        Issue.saveZeroResultsToMongoDB(new ZeroResults(
          Some(queryLoc(0)),
          Some(lineContent(2)),
          Some(lineContent(4).toInt),
          Some(lineContent(5).replace("%", "").toDouble)))

      }
    }
  }

  def updateClassificationOnDocument(issueID: String, classification: String) = Action {
    val retrieveCollection = MongoConnection()("sensis")("issues")
    val dbObject = MongoDBObject("issueID" -> issueID)
//    println(retrieveCollection.find(dbObject))
//    println(dbObject)

    val update = $set("classification" -> classification)
//    println(update)
    retrieveCollection.update(dbObject, update)

    Ok(views.html.success("asdas"))
  }

}
