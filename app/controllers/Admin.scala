package controllers

import scala.collection.JavaConversions._

import com.mongodb.casbah.Imports._
import com.mongodb.casbah.Imports.MongoDBObject

import models._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import play.api.mvc._
import views._

object Admin extends Controller {
  
   val addClassificationForm: Form[Category] = Form(
    mapping(      
      "superClassification" -> text,
      "subClassification" -> text
      ) {( superClassification , subClassification) => Category(superClassification, subClassification) } { category => Some(category.superClassification , category.subClassification) }.verifying("Error in Inserting", category => Classification.saveClassification(category)))
  
  def addClassification = Action { implicit request =>
    Ok(views.html.addClassification(addClassificationForm,Classification.retrieveClassifications))  
  }
  
  def submitClassification = Action { implicit request =>
    addClassificationForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.addClassification(formWithErrors, Classification.retrieveClassifications)),
      issue => Ok(views.html.success("Classification Has Been Created.")))
  }
  
   val deleteClassificationForm: Form[SubClassification] = Form(
    mapping(            
      "classification" -> list(text)
      ) {( subClassification) => SubClassification(subClassification) } 
    { sub => Some( sub.value) }.verifying("Error in Deleting", sub => Classification.deleteClassification(sub)))
  
      
   def deleteClassification = Action{ implicit request =>
    Ok(views.html.deleteClassification(deleteClassificationForm,Classification.retrieveClassifications))  
  }
   
  
  
  def deleteConfClassification = Action { implicit request =>
    deleteClassificationForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.deleteClassification(formWithErrors, Classification.retrieveClassifications)),
      issue => Ok(views.html.success("Classification Has Been Deleted.")))
  }
  
  def deleteIssue = TODO
}
