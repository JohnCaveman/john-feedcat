package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import models.User
import views.html.defaultpages.badRequest
import models.Issue

case class PasswordChange(username: String , originalPass: String , changedPass : String, repeatPass : String)
object Login extends Controller {

  val loginForm: Form[User] = Form(
    mapping(
      "userID" -> text,
      "pass" -> text) { (userID, pass) => User(userID, sha(pass), null) } { user => Some(user.userID, user.pass) }.verifying("Login Error", user => User.authenticate(user.userID, user.pass).isDefined))

  def getUserType(user: User): Result = {
   User.getUserType(user).userType match {
      case "Admin" => Redirect(routes.Application.issues).withSession("userType" -> "Admin" )
      case "Feedcat User" => Redirect(routes.Application.issues).withSession("userType" -> "Feedcat User")
      case _ => Redirect(routes.Login.renderLogin)
   }
  }

  def getUserFromDb(user: User): Result = {
     User.getUser(user).userType match {
      case "Admin" => Redirect(routes.Application.issues).withSession("userType" -> "Admin","firstName"-> User.getUser(user).firstName )
      case "Feedcat User" => Redirect(routes.Application.issues).withSession("userType" -> "Feedcat User","firstName"-> User.getUser(user).firstName)
      case _ => Redirect(routes.Login.renderLogin)
   }
    
  }
  
  def renderLogin = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.login(formWithErrors)),
      user => getUserFromDb(user))
  }

  def sha(s: String) = {
    val md = java.security.MessageDigest.getInstance("SHA-256")
    // add a salt. can replace salt with generated salt value
    val v = "salt" + s
    // return encoded value
    new sun.misc.BASE64Encoder().encode(md.digest(v.getBytes("UTF-8")))
  }
  
  
  val newPassForm: Form[PasswordChange] = Form(
      mapping(
      "username" -> text,  
      "oldPassword" -> text,
      "newPassword" -> text,
      "repeatPassword" -> text)
      { (username, oldPassword,newPassword,repeatPassword) => PasswordChange(username, oldPassword,newPassword,repeatPassword) }
      { user => Some(user.username, user.originalPass, user.changedPass, user.repeatPass) }.verifying("Error in changing Password", user => user.repeatPass match { case user.changedPass => User.changePassword(user.username, sha(user.changedPass), sha(user.originalPass)); case _ => false}))
  

  def passwordChanged = Action { implicit request => 
    newPassForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.changePassword(formWithErrors)),
      user =>     
        Redirect(routes.Login.changePassword).flashing("message" -> "Password Changed"))
    
  }
  
  def changePassword = Action { implicit request =>
    Ok(views.html.changePassword(newPassForm))
  }
  
  

}

trait Secured {

  def username(request: RequestHeader) = request.session.get(Security.username)

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Login.renderLogin)

  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(username, onUnauthorized) { user =>
      Action(request => f(user)(request))
    }
  }

  /**
   * This method shows how you could wrap the withAuth method to also fetch your user
   * You will need to implement UserDAO.findOneByUsername
   */
//  def withUser(f: User => Request[AnyContent] => Result) = withAuth { username => implicit request =>
//    User.getUser(username).map { user =>
//      f(user)(request)
//    }.getOrElse(onUnauthorized(request))
//  }
}