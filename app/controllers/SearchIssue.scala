package controllers

import models.Search
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import models.Issue
import models.Classification
import models.SimpleSearch

object SearchIssue extends Controller {

  val searchForm: Form[Search] = Form(
    mapping("query" -> text,
      "location" -> text,
      "date" -> text,
      "reporter" -> text,
      "classification" -> list(text),
      "issuePriority" -> optional(text),
      "issueStatus" -> optional(text),
      "issueType" -> optional(text)) { (query, location, date, reporter, classification, priority, status, issueType) => Search(query, location, date, reporter, classification, priority, status, issueType) } { search => Some(search.query, search.location, search.date, search.reporter, search.classification, search.priority, search.status, search.issueType) }.verifying("search error", search => true))

  def renderSearch = Action { implicit request =>
    Ok(views.html.search(searchForm, Classification.retrieveClassifications))
  }

  def searchIssue = Action { implicit request =>
    searchForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.success("ddasd")),
      search => Ok(views.html.issues(Search.searchIssue(search.query, search.location, search.date, search.reporter, search.classification, search.priority, search.status, search.issueType), Classification.retrieveClassifications, Search.searchIssue(search.query, search.location, search.date, search.reporter, search.classification, search.priority, search.status, search.issueType).size)))
  }

  val simpleForm: Form[SimpleSearch] = Form(
    mapping(
      "searchParam" -> text) { (searchParam) => SimpleSearch(searchParam) } { search => Some(search.param) }.verifying("search error", search => true))

  def simpleSearchIssue = Action { implicit request =>
    simpleForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.success("ddasd")),
      search => Ok(views.html.issues(Issue.simpleSearch(search.param), Classification.retrieveClassifications, Issue.simpleSearch(search.param).size)))

  }

}