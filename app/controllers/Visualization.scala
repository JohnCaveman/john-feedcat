package controllers

import play.api._
import play.api.mvc._
import com.mongodb.casbah.MongoConnection
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBList
import models.Issue
import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import play.api.libs.json.JsObject
import play.api.libs.json.Json

object Visualization extends Controller {

  def visualise = Action { implicit request =>
   Ok(views.html.visualise(Issue.priorityCount, Issue.issueTypeCount, Issue.issueStatusCount, Issue.countIssuesByReporter))
  }
  
  /*
   * FIXME: When data comes to TB level, frequently aggregation affects perfomance
   * A solution using scheduled task (Akka) to pre-aggregation and cache should be
   * applied.
   */

  def aggregateCategory( start:String, end:String ) = Action {

    val fm = new SimpleDateFormat("yyyy-MM-dd")
    
    val range = List(fm.parse(start), fm.parse(end))
    
    val m = MongoDBObject("$match"-> MongoDBObject("date" -> MongoDBObject("$gte"->range.head, "$lte"->range.last)))

//    println(m)
    
    // Filter classification:[] to classification:Un-Classified, because unwind just IGNORE empty list !
    // classification == [] ? ["Unclassified] : $classification
    val p = MongoDBObject("$project" -> MongoDBObject("classification" -> MongoDBObject("$cond" -> (MongoDBObject("$eq" -> ("$classification",  List())), List("Un-Classified"), "$classification")), "_id" -> 0))

    val unwind = MongoDBObject("$unwind" -> "$classification")

    val g = MongoDBObject("$group" -> MongoDBObject("_id" -> "$classification", "total" -> MongoDBObject("$sum" -> 1)))

    val s = MongoDBObject("$sort" -> MongoDBObject("_id" -> 1))

    val result = MongoConnection()("sensis")("issues").aggregate(m, p, unwind, g, s).results.toList

    Ok(Json.parse(com.mongodb.util.JSON.serialize(result)))
  }

  def getWeightedHeatPoints = Action {

    val coll = MongoConnection()("sensis")("issues")

    // FIXME: aggregate points in a region but not on GPS point to avoid flood G heat map 
    val p = MongoDBObject("$project" -> MongoDBObject("gps" -> "$geo.coordinates", "cc" -> "$userFeedback.count"))
    val g = MongoDBObject("$group" -> MongoDBObject("_id" -> "$gps", "weiget" -> MongoDBObject("$sum" -> "$cc")))

    val pointList = coll.aggregate(List(p, g)).results.toList
    val jsArr = com.mongodb.util.JSON.serialize(pointList)

    Ok(Json.parse(jsArr))
  }

  def aggregateStatusTrends(year: Int) = Action {

    val coll = MongoConnection()("sensis")("issues")

    // Projection flat date to year and month, match year, group on month, then status 
    val pdate = MongoDBObject("$project" -> MongoDBObject("year" -> MongoDBObject("$year" -> "$date"),
      "month" -> MongoDBObject("$month" -> "$date"),
      "status" -> "$status",
      "_id" -> 0))

    val m = MongoDBObject("$match" -> MongoDBObject("year" -> year))

    val g = MongoDBObject("$group" -> MongoDBObject("_id" -> MongoDBObject("month" -> "$month", "status" -> "$status"),
      "count" -> MongoDBObject("$sum" -> 1)))

    val result = coll.aggregate(List(pdate, m, g)).results.toList

    val jsArr = com.mongodb.util.JSON.serialize(result)

    Ok(Json.parse(jsArr))
  }

  def aggregateReporterWeekly(start: String, end: String) = Action {

    val fm = new SimpleDateFormat("yyyy-MM-dd")
    val wfm = new SimpleDateFormat("w")
    val yfm = new SimpleDateFormat("y")

    // Amazing Scala for data manipulate, in terms of that really reduce code a lot
    // List(List(1988, 13), List(2013, 07)).map => (1988.13, 2013.07)
    val range = List(start, end).map(dateStr => yfm.format(fm.parse(dateStr)).toInt + wfm.format(fm.parse(dateStr)).toDouble/100 )

//    println ("range:", range)
    // Here goes a condition when choosing feedback btw user and customer
    // Suppose one issue only contains one feedback, and customer has a higer priority
    // FIXME: if two reporters have the same name, they will collapse into one person's contribution
    val p = MongoDBObject("$project" -> MongoDBObject("time" -> MongoDBObject("$add"->( MongoDBObject("$year" -> "$date"), MongoDBObject("$divide"->(MongoDBObject("$week" -> "$date"), 100)))),
      "reporter" -> MongoDBObject("$cond" -> ("$customerFeedback.reporter", "$customerFeedback.reporter", "$userFeedback.reporter")),
      "_id" -> 0))
    
//    println("p:", p)  
      
    val m = MongoDBObject("$match" -> MongoDBObject("time" -> MongoDBObject("$gte" -> range.head, "$lte" -> range.last)) )
    
//    println("m:", m)

    val g = MongoDBObject("$group" -> MongoDBObject("_id" -> MongoDBObject("time"->"$time", "reporter"->"$reporter"), "count" -> MongoDBObject("$sum" -> 1)))
      
    val s = MongoDBObject("$sort" -> MongoDBObject("_id.time"->1))

    val result = MongoConnection()("sensis")("issues").aggregate(List(p, m, g, s)).results.toList
    val jsArr = com.mongodb.util.JSON.serialize(result)

    Ok(Json.parse(jsArr))
  }
}
