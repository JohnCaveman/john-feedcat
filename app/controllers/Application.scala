package controllers

import models.Classification
import models.Issue
import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action { implicit request =>
//    val listA = List("a","b","c")
//    println(listA.mkString(" " ))
    Ok(views.html.visualise(Issue.priorityCount, Issue.issueTypeCount, Issue.issueStatusCount, Issue.countIssuesByReporter))
  }

  def issues = Action { implicit request =>
    Ok(views.html.issues(Issue.retrieveAndSanitizeIssueWithSkip(1), Classification.retrieveClassifications, Issue.numOfDocumentsInDB))
  }

  def detailDisplay(issueID: String) = Action { implicit request =>
    Ok(views.html.detailIssueDisplay(CreateIssue.updateIssueForm, Issue.getIssue(issueID), Classification.retrieveClassifications))
  }
  def createIssue = Action { implicit request =>
    Ok(views.html.createIssue("create")).withSession(request.session + "userType" -> "Feedcat User")
  }

  def logout = Action { implicit request =>
    Redirect(routes.Login.renderLogin).withNewSession.flashing(
      "success" -> "You've been logged out")
  }

  def fetchFromdb(num: String) = Action { implicit request =>
    Ok(views.html.issues(Issue.retrieveAndSanitizeIssueWithSkip(Integer.parseInt(num)), Classification.retrieveClassifications, Issue.numOfDocumentsInDB))

  }
  def admin = Action { implicit request =>
    Ok(views.html.admin("Hello"))

  }

  def classify = Action { implicit request =>

    Ok(views.html.classificationTest(models.Classification.retrieveClassifications))

  }

   def feedBackForm = Action { implicit request =>
    Ok(views.html.anonIssueSubmit(CreateIssue.newIssueForm))
  }

  def getIssuesCount: Double = {
    Issue.numOfDocumentsInDB
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(Routes.javascriptRouter("jsRoutes")(
      routes.javascript.CreateIssue.updateClassificationOnDocument)).as("text/javascript")
  }

  def fetchZero = Action { implicit request =>
    Ok(views.html.issues(Issue.fetchZeroIssues, Classification.retrieveClassifications, Issue.numOfDocumentsInDB))

  }

  def fetchUserFeedback = Action { implicit request =>
    Ok(views.html.issues(Issue.fetchUserIssues, Classification.retrieveClassifications, Issue.numOfDocumentsInDB))

  }
  def fetchCustomerFeedback = Action { implicit request =>
    Ok(views.html.issues(Issue.fetchCustIssues, Classification.retrieveClassifications, Issue.numOfDocumentsInDB))

  }
  
  def deleteIssue(issueID : String) = Action { implicit request =>
//    println("inside delete")
  if(  Issue.deleteIssue(issueID) ){
   Ok(views.html.issues(Issue.retrieveAndSanitizeIssueWithSkip(1), Classification.retrieveClassifications, Issue.numOfDocumentsInDB))
  }
  else {
    Ok(views.html.success("Hello"))
  }
  }
  
  def reformatFunction(issueID : String) = Action {implicit request =>
    Redirect("/deploy/format/"+issueID)}
}

