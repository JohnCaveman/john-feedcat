package controllers

import models._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import views._

object Register extends Controller {

  // Method for validating 
  def register = Action { implicit request =>
    Ok(views.html.create(registerForm))
  }

  val registerForm: Form[UserRegister] = Form(
    mapping(
      "firstName" -> text,
      "lastName" -> text,
      "username" -> text,
      "password" -> text,
      "userType" -> text) { (firstName, lastName, username, password, userType) => UserRegister(firstName, lastName, username, sha(password), userType) } { user => Some(user.firstName, user.lastName, user.username, user.password, user.userType) }.verifying("Error in Registration", user => UserRegister.create(user.firstName, user.lastName, user.username, user.password, user.userType)))

  def registration = Action { implicit request =>
    registerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.create(formWithErrors)),
      user => Redirect("/register").flashing(("success" -> "The User was successfully added to FeedCAT")))
  }

  def sha(s: String) = {
    val md = java.security.MessageDigest.getInstance("SHA-256")
    // add a salt. can replace salt with generated salt value
    val v = "salt" + s
    // return encoded value 
    new sun.misc.BASE64Encoder().encode(md.digest(v.getBytes("UTF-8")))
  }

  val registerForm2 = Form(
    mapping(
      "firstName" -> text,
      "lastName" -> text,
      "username" -> text,
      "password" -> text,
      "userType" -> text)(UserRegister.apply)(UserRegister.unapply))

  def deleteUser = Action { implicit request =>
    Ok(views.html.deleteUser(deleteUserForm, User.getAllUsers))
  }
  val deleteUserForm: Form[UserToDel] = Form(
    mapping(
        "userID" -> text) { (userID) => UserToDel(userID) } { user => Some(user.userID) }.verifying("Error in Registration", user => User.delete(user.userID)))

  def delete = Action { implicit request =>
    val userInSession = session.get("User_Name")
    deleteUserForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.deleteUser(formWithErrors, User.getAllUsers)),
      user => if(session.get("User_Name").equals(user.userID)){ Redirect("/logout") }
  else Redirect("/deleteUser").flashing("success" -> "User Deleted Successfully.") 
  
    )
  }
  

//  
//  user.userID match {
//        case userInSession => Redirect(routes.Login.renderLogin)
//        case _ =>  Redirect(routes.Application.issues)
//      }
  
  
  
}