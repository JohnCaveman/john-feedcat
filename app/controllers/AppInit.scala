package controllers

import play.api._
import play.api.mvc._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoConnection
import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import scala.io.Source
import play.api.Play.current
import play.api.libs.json.Json
import play.api.libs.json.JsObject
import play.api.libs.ws.WS
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit._
import org.bson.types.ObjectId
import models.Issue
import models.Classification
import models.UserRegister

object AppInit extends Controller {

  def deploy() = TODO
  
  
  def init = Action { implicit request =>

    UserRegister.create("Delete me", "After Deploy", "sensis", Register.sha("123456"), "Admin")
    
    val sensisDB = MongoConnection()("sensis")

    // Drop and create classification
    sensisDB("classification").drop

    val json = Json.parse(Source.fromFile(Play.getFile("conf/data/classification.json")).mkString)

    json.as[List[JsObject]].foreach(
      category =>
        sensisDB("classification").insert(com.mongodb.util.JSON.parse(category.toString).asInstanceOf[DBObject]))

    // Enable text search 
    MongoConnection()("admin").command(MongoDBObject("setParameter" -> 1, "textSearchEnabled" -> 1))

    // Drop and create issues and ensure Index on TEXT everything and DATE
    // FIXME: According to Mongo full TEXT index will cost A LOT both in storage and performance
    sensisDB("issues").drop()
    sensisDB.createCollection("issues", MongoDBObject()).ensureIndex("date")
    sensisDB("issues").ensureIndex(MongoDBObject("$**" -> "text"), MongoDBObject("name" -> "text_index_for_everything"))

    Ok("MongoDB: Read and fill from classification.json, drop issues, enable text search, build index\nMySQL: created default user")
  }


  def formatIssueModel(id: String) = Action { implicit request =>

    var log = "Start convert id: " + id
    val issuesColl = MongoConnection()("sensis")("issues")

    /*
     * Add additional geo-location field for map visualization
     * Thanks god we can convert DBObject to MongoDBObject and utilize scala type match 
     */
    val g_ws = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address="

    issuesColl.findOne( MongoDBObject("issueID"->id) ) match {

      case None => log ++= "\nCannot find document with that id"

      case Some(tbCast) =>

        tbCast.getOrElse("geo", {

          log ++= "\nGeo field does not exists, converting from location"

          val sources = List("userFeedback", "customerFeedback", "zeroResult").filter(each => tbCast.keySet().contains(each))

          if (sources.size == 0) log ++= ", but no avaliable sources found"

          sources.foreach(source => {

            log ++= "\nFound sourse: " + source + ", start convert with that kind of feedback"

            // should never throw error here even use get
            tbCast.get(source).asInstanceOf[DBObject].getAs[String]("location") match {

              case None => log ++= ", but no location found"

              case Some(loc) => {
                // use google web services to get location, since free limit is 2500 per day
                // maybe a mapping file should be provided by sensis

                var literalLocation = loc match {

                  case "All States" => "Australia"
                  case _ => loc.replace(' ', '+')
                }

                val query = g_ws + literalLocation

                val future = WS.url(query).get.map { response => response.json \\ "location" }

                val coordinator = Await.result(future, FiniteDuration(5000, MILLISECONDS))

                val geoJson = Json.arr(coordinator(0) \ "lat", coordinator(0) \ "lng")

                if (geoJson.value.size == 2) {

                  // Finish calling ws, start to insert into documents: 
                  // geo: { type: "Point", coordinates: [100.0, 0.0] }
                  val geo = MongoDBObject("type" -> "Point",
                    "coordinates" -> com.mongodb.util.JSON.parse(geoJson.toString))

                  tbCast.put("geo", geo)
                  issuesColl.save(tbCast)
                  log ++= "\nSuccessfully convert\n"
                } else {
                  log ++= "\nParse googleAPI result error, daily limit? network?\n"
                }

              }
            }
          })
        })
    }
    
    println(log + "\nEnd of converting id: " + id)
    Ok(views.html.issues(Issue.retrieveAndSanitizeIssueWithSkip(1), Classification.retrieveClassifications, Issue.numOfDocumentsInDB))

  }
}
