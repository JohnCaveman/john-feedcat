import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "feedCAT"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "mysql" % "mysql-connector-java" % "5.1.26",
    "org.mongodb" %% "casbah" % "2.6.2",
    "net.sf.opencsv" % "opencsv" % "2.1",
    "org.webjars" % "jquery" % "1.8.3" 
//    "se.radley" %% "play-plugins-salat" % "1.3.0"
    
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here   
//      routesImport += "se.radley.plugin.salat.Binders._",
//      templatesImport += "org.bson.types.ObjectId"
     // routesImport += "se.radley.plugin.salat.Binders._",
  //templatesImport += "org.bson.types.ObjectId"
  )

}
